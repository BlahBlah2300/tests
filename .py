#!/usr/bin/env python3
from time import sleep #dot is one unit of time, dash is 3 units, gaps between dots and dashes = 1 unit, gaps between characters = 3 units, gaps between words = 7 units
import skilstak.colors as c
import skilstak.pylights as pylights #simulates a real light, changing colors simulates being on/off
unit = .1
morse = {
        'a': '.-',
        'b': '-...',
        'c': '-.-.',
        'd': '-..',
        'e': '.',
        'f': '..-.',
        'g': '--.',
        'h': '....',
        'i': '..',
        'j': '.---',
        'k': '-.-',
        'l': '.-..',
        'm': '--',
        'n': '-.',
        'o': '---',
        'p': '.--.',
        'q': '--.-',
        'r': '.-.',
        's': '...',
        't': '-',
        'u': '..-',
        'v': '...-',
        'w': '.--',
        'x': '-..-',
        'y': '-.--',
        'z': '--..',
        '1': '.----',
        '2': '..---',
        '3': '...--',
        '4': '....-',
        '5': '.....',
        '6': '-....',
        '7': '--...',
        '8': '---..',
        '9': '----.',
        '0': '-----',
        ' ': '/'}
#spaces are gaps between characters, / means a gap between words
lightcode = {
            '.': '11',
            '-': '13',
            ' ': '02',
            '/': '06'}
#The lightcode dictionary returns on/off signals and timing units for character
def returncode(msg): #converts message into code
    codemsg = ''
    for x in msg:
        if morse[x] == '/':
            codemsg = codemsg[:-1]
        codemsg += morse[x]
        if morse[x] != '/':
            codemsg += ' '
    return(codemsg)
def lights(msg): #simulates flashing lights
    code = returncode(msg)
    lights = pylights.Set()
    lights.update()
    lights[0].color = c.b03
    lights.update()
    for x in code:
        if lightcode[x][0] == 0:
            lights[0].color = c.b03
            lights.update()
            sleep(int(lightcode[x][1])*unit)
        else:
            lights[0].color = c.red
            lights.update()
            sleep(int(lightcode[x][1])*unit)
            lights[0].color = c.b03
            lights.update()
            sleep(unit)
lights('dank memes') #calling the function